rockola-kicad-footprints
================================

Generates KiCAD footprints programmatically.

## Requirements 

* Python 3 (>3.5)
* KiCad Footprint Generator (KicadModTree) - https://gitlab.com/kicad/libraries/kicad-footprint-generator

## Included footprints

* Aries Series 55 ZIF sockets, datasheet: https://www.mouser.fi/datasheet/2/35/10001-universal-dip-zif-test-socket-337360.pdf

## TODO

* Rename make_footprints.py to something else.
* Separate different footprint families to different source files (assuming there will be more than one at some point).
* Get output directory (hardcoded for now) from e.g. command line parameter 
* Aries Series 55 ZIF sockets: datasheet doesn't cover all dimensions (e.g. handle size), measure a real example for better guesses (but these might of course change at the whim of the manufacturer)
