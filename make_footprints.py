#!/usr/bin/env python

import math

from KicadModTree import *
from pathlib import Path

footprint_directory = './output/'

def aries_series_55():
    datasheet = {
        'revision':  '2.0',
        'url': 'https://www.mouser.fi/datasheet/2/35/10001-universal-dip-zif-test-socket-337360.pdf'
        }
    n_pins = [24, 28, 32, 36, 40, 42, 44, 48]
    spacings = { 3: 7.62, 6: 15.42 } # 0.3", 0.6"
    # handle: 3 = left down is closed, 4 = right down is closed (std)
    #
    # note:
    # 1 = left up is closed, 2 = right up is closed
    # but these have identical footprints
    handles = { 3: 'left', 4: 'right' }
    f_silk = 'F.SilkS'
    f_fab = 'F.Fab'
    f_courtyard = 'F.CrtYd'
    width = 22.85
    for spacing in spacings.keys():
        mm_spacing = spacings[spacing]
        for handle in handles.keys():
            for pins in n_pins:
                # spec_a, spec_b, spec_c == "A","B","C" on datasheet
                row_pins = pins // 2
                spec_a = 14.99 + row_pins * 2.54
                spec_b = (row_pins - 1) * 2.54
                spec_c = row_pins * 2.54
                code= f'{pins}-{spacing}55{handle}-1X'
                name=f'ZIF_Aries_S55_{code}'
                description = f'Aries Series 55 ZIF socket {code}, {pins} pins, {mm_spacing}mm row-to-row spacing, {handles[handle]} down is closed'
                print(name, ' - ', description)
                kicad_mod = Footprint(name)
                kicad_mod.setDescription(description)
                kicad_mod.setTags("zif")
                # pins
                for pin in range(1, pins // 2 +1):
                    shape=Pad.SHAPE_CIRCLE
                    if pin == 1:
                        shape=Pad.SHAPE_RECT
                    pad_size = [2, 2]
                    # datasheet "suggested PCB hole size"
                    # .032" +/- .002" = 0.81...0.82
                    drill=0.9
                    y = (pin - 1) * 2.54
                    # leftmost pin
                    x1 = 0
                    kicad_mod.append(Pad(number=pin, type=Pad.TYPE_THT, shape=shape, at=[x1, y], size=pad_size, drill=drill, layers=Pad.LAYERS_THT))
                    # rightmost pin
                    x2 = mm_spacing
                    pin2 = pins - pin + 1
                    kicad_mod.append(Pad(number=pin2, type=Pad.TYPE_THT, shape=Pad.SHAPE_CIRCLE, at=[x2, y], size=pad_size, drill=drill, layers=Pad.LAYERS_THT))
                if spacing == 3:
                    other_spacing = 6
                else:
                    other_spacing = 3
                left = (mm_spacing - width) / 2.0
                top = spec_b + 10.54 - spec_a
                right = left + width
                bottom = top + spec_a
                print(f'RectLine(start=[{left}, {top}], end=[{right}, {bottom}], layer={f_silk}')
                kicad_mod.append(RectLine(start=[left, top], end=[right, bottom], layer=f_silk))
                x_margin = 0.25
                y_margin = 0.25
                cy_left = left - x_margin
                cy_right = right + x_margin
                cy_top = top - y_margin
                cy_bottom = bottom + y_margin
                kicad_mod.append(RectLine(start=[cy_left, cy_top], end=[cy_right, cy_bottom], layer=f_courtyard))
                #
                ref_x = mm_spacing / 2
                ref_y = top - 1
                kicad_mod.append(Text(type='user', text='REF**', at=[ref_x, ref_y], layer=f_fab))
                kicad_mod.append(Text(type='reference', text='REF**', at=[ref_x, ref_y], layer=f_silk))
                # Screw holes at both ends
                r = 1.65 / 2.0
                hole_offset = 2.21
                y = top + hole_offset
                kicad_mod.append(Circle(center=[ref_x, y], radius=r, layer=f_silk))
                y = bottom - hole_offset
                kicad_mod.append(Circle(center=[ref_x, y], radius=r, layer=f_silk))
                #
                # handle consists of rectangular (cylindrical really) stem
                # and teardrop-shaped knob
                handle_width = 1.2 # just a guess, not specified on datasheet
                handle_length = 12 # guess, not including knob
                knob_radius = 1.5 # guess
                knob_length = 4 # guess
                knob_layer = f_fab
                if handle == 3:
                    x = left
                else:
                    x = right - handle_width
                y = bottom + 11.05 - handle_length - knob_length
                kicad_mod.append(RectLine(start=[x, y],
                                          end=[x + handle_width, y + handle_length],
                                          layer=knob_layer))
                knob_x = x + (handle_width / 2.0)
                knob_y = y + handle_length + (knob_length - knob_radius)
                kicad_mod.append(Arc(center=[knob_x, knob_y],
                                     angle=180.0,
                                     midpoint=[knob_x, knob_y + knob_radius],
                                     layer=knob_layer))
                kicad_mod.append(Line(start=[knob_x + knob_radius, knob_y],
                                      end=[knob_x + (handle_width / 2.0), y + handle_length],
                                      layer=knob_layer))
                kicad_mod.append(Line(start=[knob_x - knob_radius, knob_y],
                                      end=[knob_x - (handle_width / 2.0), y + handle_length],
                                      layer=knob_layer))
                #
                #
                kicad_mod.append(Text(type='value', text=name, at=[ref_x, bottom + 2], layer=f_fab))
                # Done, saving to disk
                file_handler = KicadFileHandler(kicad_mod)
                outputdir = Path(footprint_directory)
                outputdir.mkdir(parents=True, exist_ok=True)
                outputfile = outputdir / f'{name}.kicad_mod'
                file_handler.writeFile(str(outputfile))

aries_series_55()
